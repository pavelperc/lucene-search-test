﻿using System;
using Lucene.Net.Analysis.Synonym;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers.Classic;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Lucene.Net.Util;

namespace LuceneTest {

public static class AppLucene {
    public const LuceneVersion Version = LuceneVersion.LUCENE_48;
}


internal class Program {
    public static void Main(string[] args) {
        // ------ Create an index and define a text analyzer
        

        var indexLocation = "../../LuceneIndex";
        var dir = FSDirectory.Open(indexLocation);

        //create an analyzer to process the text
        var analyzer = new MyRussianAnalyzer();
        
        //create an index writer
        var indexConfig = new IndexWriterConfig(AppLucene.Version, analyzer);
        var writer = new IndexWriter(dir, indexConfig);
        
        // ------ Add to the index 
        
        FillDocuments(writer);
        writer.Flush(triggerMerge: false, applyAllDeletes: false);
        writer.Commit();
        

        // ------ Construct a query
        QueryParser parser = new QueryParser(AppLucene.Version, "answer", analyzer);

        // Console.Write("Enter your query: ");
        // string queryStr = Console.ReadLine();
        // string queryStr = "ДТП в Алматы";
        // string queryStr = "кот";
        string queryStr = "пирог с котом";
        Console.WriteLine("Query: " + queryStr);
        Console.WriteLine();
        Query query = parser.Parse(queryStr);

        // re-use the writer to get real-time updates
        DirectoryReader ireader = DirectoryReader.Open(dir);
        // var searcher = new IndexSearcher(writer.GetReader(applyAllDeletes: true));
        IndexSearcher searcher = new IndexSearcher(ireader);
        var hits = searcher.Search(query, 20 /* top 20 */).ScoreDocs;
        Console.WriteLine("Hits: " + hits.Length);
        foreach (var hit in hits) {
            var foundDoc = searcher.Doc(hit.Doc);
            Console.WriteLine("Score: " + hit.Score);
            Console.WriteLine("Question: " + foundDoc.Get("question"));
            string answer = foundDoc.Get("answer");
            if (answer.Length > 100) {
                answer = answer.Substring(0, 100) + "...";
            }
            Console.WriteLine("Answer: " + answer);
        }
    }

    private static Document CreateFaqDocument(string question, string answer) {
        var questionField = new TextField("question", question, Field.Store.YES);
        var answerField = new TextField("answer", answer, Field.Store.YES);
        
        var document = new Document();
        document.Add(questionField);
        document.Add(answerField);
        return document;
    }
    
    private static void FillDocuments(IndexWriter writer) {
        writer.DeleteAll();
        
        // var jsonStr = File.ReadAllText("../../exampleData.json");
        // foreach (var jObject in JArray.Parse(jsonStr).Children<JObject>()) {
            // Console.WriteLine($"Adding title: {jObject["title"]}");
            // Document document = CreateFaqDocument(jObject["title"].ToString(), jObject["body"].ToString());
            // writer.AddDocument(document);
        // }
        writer.AddDocument(CreateFaqDocument("Как зовут собаку?", "собаку зовут котик"));
        writer.AddDocument(CreateFaqDocument("Как зовут попугая?", "попугая зовут кошка и кошечка"));
        
    }
}
}
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Core;
using Lucene.Net.Analysis.Miscellaneous;
using Lucene.Net.Analysis.Snowball;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Analysis.Synonym;
using Lucene.Net.Analysis.Util;
using Lucene.Net.Tartarus.Snowball.Ext;
using Lucene.Net.Util;

namespace LuceneTest {
public sealed class MyRussianAnalyzer : StopwordAnalyzerBase {
    /// <summary>File containing default Russian stopwords.</summary>
    public const string DEFAULT_STOPWORD_FILE = "russian_stop.txt";

    private readonly CharArraySet stemExclusionSet;

    /// <summary>
    /// Returns an unmodifiable instance of the default stop-words set.
    /// </summary>
    /// <returns> an unmodifiable instance of the default stop-words set. </returns>
    public static CharArraySet DefaultStopSet {
        get { return DefaultSetHolder.DEFAULT_STOP_SET; }
    }

    public MyRussianAnalyzer()
        : this(DefaultSetHolder.DEFAULT_STOP_SET) {
    }

    private SynonymMap _synonymMap;

    /// <summary>Builds an analyzer with the given stop words</summary>
    /// <param name="matchVersion">lucene compatibility version</param>
    /// <param name="stopwords">a stopword set</param>
    public MyRussianAnalyzer(CharArraySet stopwords)
        : this(stopwords, CharArraySet.EMPTY_SET) {
    }

    /// <summary>Builds an analyzer with the given stop words</summary>
    /// <param name="matchVersion">lucene compatibility version</param>
    /// <param name="stopwords">a stopword set</param>
    /// <param name="stemExclusionSet"> a set of words not to be stemmed </param>
    public MyRussianAnalyzer(
        CharArraySet stopwords,
        CharArraySet stemExclusionSet
        )
        : base(AppLucene.Version, stopwords) {
        this.stemExclusionSet =
            CharArraySet.UnmodifiableSet(CharArraySet.Copy(AppLucene.Version, stemExclusionSet));

        _synonymMap = LoadSynonymMap();
    }

    private SynonymMap LoadSynonymMap() {
        Analyzer analyzer = Analyzer.NewAnonymous((fieldName, reader) => {
            var tokenizer = new WhitespaceTokenizer(AppLucene.Version, reader);
            // add russian stemming for synonyms
            var tokenStream = new SnowballFilter(tokenizer, new RussianStemmer());
            return new TokenStreamComponents(tokenizer, tokenStream);
        });
        
        var textStream = File.OpenText("../../synonyms_ru.txt");
        var parser = new SolrSynonymParser(true, true, analyzer);
        parser.Parse(textStream);
        return parser.Build();
    }
    
    protected override TokenStreamComponents CreateComponents(
        string fieldName,
        TextReader reader) {
        Tokenizer source = new StandardTokenizer(m_matchVersion, reader);
        TokenStream tokenStream = new StopFilter(m_matchVersion,
            new LowerCaseFilter(m_matchVersion, new StandardFilter(m_matchVersion, source)), m_stopwords);
        
        if (stemExclusionSet.Count > 0)
            tokenStream = new SetKeywordMarkerFilter(tokenStream, stemExclusionSet);
        tokenStream = new SnowballFilter(tokenStream, new RussianStemmer());
        
        if (_synonymMap != null) {
            tokenStream = new SynonymFilter(tokenStream, _synonymMap, true);
        }
        
        return new TokenStreamComponents(source, tokenStream);
    }

    private class DefaultSetHolder {
        internal static readonly CharArraySet DEFAULT_STOP_SET = LoadDefaultStopSet();

        private static CharArraySet LoadDefaultStopSet() {
            try {
                return WordlistLoader.GetSnowballWordSet(
                    IOUtils.GetDecodingReader(typeof(SnowballFilter), "russian_stop.txt", Encoding.UTF8),
                    LuceneVersion.LUCENE_CURRENT);
            }
            catch (IOException ex) {
                throw new Exception("Unable to load default stopword set", ex);
            }
        }
    }
}
}